package fr.jnda.android.game.ptibac

import android.content.SharedPreferences
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import android.os.CountDownTimer
import android.text.format.DateUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.NumberPicker
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.lifecycle.Lifecycle
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*


class MainActivity : AppCompatActivity() {

    private val lettersBase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    private var letters = lettersBase
    private var mJob: Job? = null
    private var isPlaying = false
    private var timePlaying = 0
    private var countDownTimer: CountDownTimer? = null
    private lateinit var prefs: SharedPreferences
    private val timeKey = "timeParty"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        timePlaying = prefs.getInt(timeKey, 0)
        setContentView(R.layout.activity_main)
        val minutes = timePlaying / 60
        val seconds = timePlaying % 60
        setTimePartyText(minutes,seconds)
        id_list_letter.text = letters
        content_main.setOnClickListener { randomLetter() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.action_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.id_action_timer -> {
                onCreateDialog()
            }
            R.id.id_action_stop -> {
                if (mJob?.isActive == true)
                    mJob?.cancel()
                countDownTimer?.cancel()
                id_countdown.text = ""
                isPlaying = false
            }

            R.id.id_action_reset -> {
                letters = lettersBase
                id_list_letter.text = lettersBase
                timePlaying = 0
                if (mJob?.isActive == true)
                    mJob?.cancel()
                countDownTimer?.cancel()
                id_countdown.text = ""
                id_letter.text = ""
                prefs.edit {
                    remove(timeKey)
                }
                setTimePartyText(0,0)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @Synchronized private fun randomLetter(){
        if( isPlaying || (mJob != null && (mJob!!.isActive || !mJob!!.isCompleted)))
            return
        mJob =GlobalScope.launch(Dispatchers.Main){
            if (letters.isNotEmpty()) {
                var attente = 25L
                repeat(25) {
                    delay(attente)
                    id_letter.text = letters.random().toString()
                    attente += 5
                }
                val tmp = id_letter.text.toString()
                letters = letters.replace(tmp, "")
                id_list_letter.text = letters
                if (timePlaying > 0)
                    startCounter()
            }
        }
    }

    @Synchronized private fun startCounter(){
        isPlaying = true
        countDownTimer = object : CountDownTimer(timePlaying.toLong() * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val reste = millisUntilFinished / 1000
                id_countdown.text =  DateUtils.formatElapsedTime(reste)
                if (millisUntilFinished < 5000 && enableSound()){
                    val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                    toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)
                }
            }

            override fun onFinish() {
                id_countdown.text = getString(R.string.count_done)
                isPlaying = false
                if (enableSound()) {
                    val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                    toneGen1.startTone(ToneGenerator.TONE_SUP_RINGTONE, 400)
                }
            }
        }.start()
    }

    private fun setTimePartyText(min:Int, sec: Int){
        if (min < 1 && sec < 1)
            id_time_party.text = resources.getString(R.string.unlimited)
        else {
            var text = getString(R.string.round_time)
            if (min > 0 ){
               text +=  resources.getQuantityString(R.plurals.minutes, min, min)
            }
            if(sec > 0){
                text += resources.getQuantityString(R.plurals.seconds, sec, sec)
            }
            id_time_party.text = text
        }
    }

    private fun enableSound() = this.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)

    private fun onCreateDialog() {
        val inflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.minutes_secondes, null)

        val minutesPicker = dialogView.findViewById<NumberPicker>(R.id.minutes)
        minutesPicker.maxValue = 3
        minutesPicker.minValue = 0
        minutesPicker.value = timePlaying / 60

        val secondesPicker = dialogView.findViewById<NumberPicker>(R.id.secondes)
        secondesPicker.maxValue = 59
        secondesPicker.minValue = 0
        secondesPicker.value = timePlaying % 60



        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setView(dialogView)
        builder.setTitle(getString(R.string.define_duration))
        builder.setPositiveButton(android.R.string.ok) { _, _ ->
            val tmp = minutesPicker.value
            val tmp2 = secondesPicker.value
            timePlaying = (tmp*60)+tmp2
            prefs.edit {
                putInt(timeKey, timePlaying)
            }
            setTimePartyText(tmp,tmp2)
        }
        builder.setNegativeButton(android.R.string.cancel) { _, _ -> }
        builder.create()
        builder.show()
    }
}